// Copyright (C) 2019  Wilko Manger
//
// This file is part of Version.
//
// Version is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Version is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Version.  If not, see <https://www.gnu.org/licenses/>.

const express = require('express')
const fetch = require('node-fetch')
const app = express()
 
var base = 'https://git.pattle.im/api/v4'

var headers = {
  'PRIVATE-TOKEN': process.env.ACCESS_TOKEN
}

app.get('/', async function (req, res) {
  var tagsResponse = await fetch(`${base}/projects/17/repository/tags`, {
    headers: headers
  })

  var latestTag = (await tagsResponse.json())[0]
  var sha = latestTag.commit.id

  var pipelinesResponse = await fetch(`${base}/projects/17/pipelines?sha=${sha}`, {
    headers: headers
  })

  var pipeline = (await pipelinesResponse.json())[0]

  var version = latestTag.name.substring(1)
  var versionCode = 123 + pipeline.id
  res.type('text').send(`${version} ${versionCode}`)
})
 
app.listen(80)
