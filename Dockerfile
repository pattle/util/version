FROM node:current-alpine AS build

COPY . . 

RUN yarn install

EXPOSE 80

ENTRYPOINT [ "node", "index.js" ]